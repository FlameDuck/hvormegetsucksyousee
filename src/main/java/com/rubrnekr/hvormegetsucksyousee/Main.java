package com.rubrnekr.hvormegetsucksyousee;

import com.rubrnekr.hvormegetsucksyousee.operation.Ping;

import java.util.concurrent.*;

/**
 * Created by FlameDuck on 09-02-2016.
 */
public class Main {
    public static void main(String[] args) throws InterruptedException {
        ScheduledExecutorService threadpool = Executors.newScheduledThreadPool(8);
        threadpool.scheduleWithFixedDelay(new Ping("www.eb.dk"),0, 1, TimeUnit.MINUTES);
        threadpool.scheduleWithFixedDelay(new Ping("www.dr.com"),0, 1, TimeUnit.MINUTES);
        threadpool.scheduleWithFixedDelay(new Ping("www.reddit.com"),0, 1, TimeUnit.MINUTES);
        threadpool.scheduleWithFixedDelay(new Ping("www.facebook.com"),0, 1, TimeUnit.MINUTES);
        threadpool.scheduleWithFixedDelay(new Ping("www.google.com"),0, 1, TimeUnit.MINUTES);
        Thread.sleep(1);
    }
}
