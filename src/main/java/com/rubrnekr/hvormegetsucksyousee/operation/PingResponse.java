package com.rubrnekr.hvormegetsucksyousee.operation;

/**
 * Created by FlameDuck on 09-02-2016.
 */
public class PingResponse {
    private final String hostname;
    private final boolean couldResolve;
    private final boolean couldConnect;
    private final long pingTime;

    public PingResponse(String hostname, boolean couldResolve, boolean couldConnect, long pingTime) {
        this.hostname = hostname;
        this.couldResolve = couldResolve;
        this.couldConnect = couldConnect;
        this.pingTime = pingTime;
    }

    @Override
    public String toString() {
        return "PingResponse{" +
                "hostname='" + hostname + '\'' +
                ", couldResolve=" + couldResolve +
                ", couldConnect=" + couldConnect +
                ", pingTime=" + pingTime +
                '}';
    }
}
