package com.rubrnekr.hvormegetsucksyousee.operation;

import org.apache.commons.lang3.time.StopWatch;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * Created by FlameDuck on 09-02-2016.
 */
public class Ping implements Runnable{

    private String hostName;
    private Optional<InetAddress> address = Optional.empty();
    private Optional<PingResponse> lastPingResponse;

    public Ping(String hostName) {
        this.hostName = hostName;
    }

    public void run() {

        try {
            address = Optional.ofNullable(InetAddress.getByName(hostName));
        } catch (UnknownHostException e) {
            lastPingResponse = Optional.of(new PingResponse(hostName, false, false, 0));
            return;
        }

        StopWatch sw = new StopWatch();
        sw.start();
        try (Socket soc = new Socket()) {
            soc.connect(new InetSocketAddress(address.get(), 80), 100);
            sw.stop();
            lastPingResponse = Optional.of(new PingResponse(hostName, true, true, sw.getTime()));
        } catch (IOException e) {
            sw.stop();
            lastPingResponse = Optional.of(new PingResponse(hostName, true, false, sw.getTime()));
        }
        if (lastPingResponse.isPresent())
        {
            System.out.println(lastPingResponse.get());
        }
    }
}
